FROM python:3.6-slim
ENV FLASK_APP=/app/chat.py 
WORKDIR /app
COPY requirements.txt .
RUN pip3 install -r requirements.txt
COPY chatapp/ .
EXPOSE 5000
CMD ["flask", "run", "-h", "0.0.0.0"]
