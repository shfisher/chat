CREATE TABLE user
(
  username VARCHAR(30) NOT NULL,
  PRIMARY KEY (username)
);

CREATE TABLE room
(
  room_id VARCHAR(30) NOT NULL,
  PRIMARY KEY (room_id)
);

CREATE TABLE message
(
  text VARCHAR(255) NOT NULL,
  id INT auto_increment NOT NULL,
  datetime DATETIME NOT NULL,
  username VARCHAR(30) NOT NULL,
  room_id VARCHAR(30) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (username) REFERENCES user(username),
  FOREIGN KEY (room_id) REFERENCES room(room_id)
);

