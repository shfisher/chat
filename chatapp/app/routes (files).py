from flask import render_template, request
from app import app
from datetime import datetime

@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')

@app.route('/<room>', methods=['POST','GET'])
def chatRoom(room):
    return render_template('index.html')
    
@app.route('/api/chat/<room>', methods=['GET'])
def loadChat(room):
	result = ""
	with open(f"rooms/{room}.csv", "a+"):
		pass
	with open(f"rooms/{room}.csv") as file:
#		file.readline()
		for line in file:
			sl = line.split(",")
			result += f"[{sl[0]} {sl[1]}] {sl[2]}: {sl[3]}"
	return result

@app.route('/api/chat/<room>', methods=['POST'])
def storeMessage(room):
	data = request.form
	dateTimeNow = datetime.now()
	day=dateTimeNow.strftime("%Y-%m-%d")
	time=dateTimeNow.strftime("%H:%M:%S")
	chatLine = f"{day},{time},{data['username']},{data['msg']}\n"
	with open(f"rooms/{room}.csv", "a+") as file:
		file.write(chatLine)
	return ""
		
			
		
