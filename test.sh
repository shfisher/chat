#!/bin/bash
random=$(curl -s "http://www.randomnumberapi.com/api/v1.0/random?min=9999&max=99999&count=1" | cut -c 2-6)

curl -X POST -d "username=user${random}&msg=msg${random}" http://$1/api/chat/room1
sleep 5
curl -X GET http://$1/api/chat/room1 | grep "user${random}: msg${random}"
