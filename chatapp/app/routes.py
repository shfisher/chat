from flask import render_template, request
from app import app
from datetime import datetime
import mysql.connector, os 
from dotenv import load_dotenv

load_dotenv()
USER=os.environ.get('MYSQL_USER')
PASSWORD=os.environ.get('MYSQL_PASSWORD')
HOST=os.environ.get('MYSQL_HOST')
DB=os.environ.get('MYSQL_DATABASE')

@app.route('/')
@app.route('/index')
def index():
    return app.send_static_file('index.html')

@app.route('/<room>', methods=['POST','GET'])
def chatRoom(room):
    return app.send_static_file('index.html')

@app.route('/api/chat/<room>', methods=['GET'])
def loadChat(room):
	chatLog = ""
	conn = mysql.connector.connect(user=USER, password=PASSWORD, host=HOST, database=DB)
	cur = conn.cursor(buffered=True)
	cur.execute(f"SELECT datetime,username,text FROM message WHERE room_id='{room}';")
	if cur.rowcount:
		selectResult=cur.fetchall()
		for row in selectResult:
			chatLog += f"[{row[0]}] {row[1]}: {row[2]}\n"
	cur.close()
	return chatLog

@app.route('/api/chat/<room>', methods=['POST'])
def storeMessage(room):
	data = request.form
	dateTimeNow = datetime.now()
	msgDateTime = dateTimeNow.strftime("%Y-%m-%d %H:%M:%S")
	username=data['username']
	msg=data['msg']
	
	conn = mysql.connector.connect(user=USER, password=PASSWORD, host=HOST, database=DB)
	cur = conn.cursor(buffered=True)
	#check and create room and data['username']
	cur.execute("SELECT * FROM user WHERE username=%s;", (username,))
	if not cur.rowcount:
		cur.execute("INSERT INTO user (username) VALUES (%s);", (username,))
		conn.commit()
	cur.execute(f"SELECT * FROM room WHERE room_id='{room}';")
	if not cur.rowcount:
		cur.execute(f"INSERT INTO room (room_id) VALUES ('{room}');")
		conn.commit()
	cur.execute(f"INSERT INTO message (text, datetime, username, room_id) VALUES ('{msg}','{msgDateTime}','{username}','{room}');")
	conn.commit()
	cur.close()

	return ""
